// // Module and Class
// (function () {
//     class Textarea {
//         constructor(mesageAreaId, symbolsCountId, remainingSymbolsCount) {
//             this.defaultSymbolsCount = 140;
//             this.messageArea = document.getElementById(mesageAreaId);
//             this.symbolsCount = document.getElementById(symbolsCountId);
//             this.remainingSymbolsCount = document.getElementById(remainingSymbolsCount);
//         }
//     }
//
//     Textarea.prototype.init = function () {
//         this.remainingSymbolsCount.innerHTML = this.defaultSymbolsCount;
//         this.addKeypressEventListeners();
//     };
//
//     Textarea.prototype.addKeypressEventListeners = function () {
//         let self = this;
//         let defaultSymbolsCount = self.defaultSymbolsCount;
//         let allowedKeyCodes = [8, 35, 36, 37, 38, 39, 40, 45, 46];
//         let allowedKey;
//
//         /*Keydown event*/
//         self.messageArea.addEventListener('keydown', function (e) {
//             allowedKeyCodes.indexOf(e.keyCode) > -1 ? allowedKey = true : allowedKey = false;
//
//             if (this.value.length >= defaultSymbolsCount && !allowedKey) {
//                 e.preventDefault();
//             }
//
//             allowedKey = false;
//         });
//
//         /*Keyup event*/
//         self.messageArea.addEventListener('keyup', function () {
//             let symbolsCount = this.value.length;
//
//             if (symbolsCount > defaultSymbolsCount) {
//                 symbolsCount = defaultSymbolsCount;
//             }
//
//             self.symbolsCount.innerHTML = symbolsCount;
//             self.remainingSymbolsCount.innerHTML = defaultSymbolsCount - symbolsCount;
//         });
//     };
//
//     let instance = new Textarea('js-message', 'js-message-left-total', 'js-message-left-symbols');
//     instance.init();
// })();


// JS Object Architecture
let TextCounter = {
    maxChar: 140,
    textArea: document.querySelector('#js-message'),
    textAreaTotal: document.querySelector('#js-message-left-total'),
    textAreaLeft: document.querySelector('#js-message-left-symbols'),

    init: function () {
        // const allowedKeyCodes = [8, 35, 36, 37, 38, 39, 40, 45, 46];
        // let allowedKey;
        // /*Keydown event*/
        // this.textArea.addEventListener('keydown', function (e) {
        //     allowedKeyCodes.indexOf(e.keyCode) > -1 ? allowedKey = true : allowedKey = false;
        //
        //     if (this.value.length >= self.maxChar && !allowedKey) {
        //         e.preventDefault();
        //     }
        //
        //     allowedKey = false;
        // });
        //
        // /*Keyup event*/
        // this.textArea.addEventListener('keyup', function () {
        //     let symbolsCount = this.value.length;
        //
        //     if (symbolsCount > self.maxChar) {
        //         symbolsCount = self.maxChar;
        //     }
        //
        //     self.textAreaTotal.innerHTML = symbolsCount;
        //     self.textAreaLeft.innerHTML = self.maxChar - symbolsCount;
        // });

        const self = this;

        this.textArea.addEventListener('input', function () {
            if(this.value.length > self.maxChar) {
                this.value = this.value.substr(0, self.maxChar);
            }
        });
    },
};

TextCounter.init();